import pandas as pd
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt


"""
run this at the start: `pip install requirements.txt`
"""

df_bookings = pd.read_csv("./data.csv", sep=";", header=0)


print(df_bookings.describe())


# 1. Feature engineering

df_bookings['datum_dolaska'] = pd.to_datetime(df_bookings['datum_dolaska'], format='%d.%m.%Y')
df_bookings['datum_kreiranja_rezervacije'] = pd.to_datetime(df_bookings['datum_kreiranja_rezervacije'], format='%d.%m.%Y')
df_bookings['datum_odjave'] = pd.to_datetime(df_bookings['datum_odjave'], format='%d.%m.%Y')
df_bookings['datum_otkazivanja_rezervacije'] = pd.to_datetime(df_bookings['datum_otkazivanja_rezervacije'], format='%d.%m.%Y')

df_bookings['datum_dolaska_y'] = df_bookings['datum_dolaska'].apply(lambda x: x.year)
df_bookings['datum_dolaska_m'] = df_bookings['datum_dolaska'].apply(lambda x: x.month)
df_bookings['datum_dolaska_d'] = df_bookings['datum_dolaska'].apply(lambda x: x.day)

df_bookings['datum_kreiranja_rezervacije_y'] = df_bookings['datum_kreiranja_rezervacije'].apply(lambda x: x.year)
df_bookings['datum_kreiranja_rezervacije_m'] = df_bookings['datum_kreiranja_rezervacije'].apply(lambda x: x.month)
df_bookings['datum_kreiranja_rezervacije_d'] = df_bookings['datum_kreiranja_rezervacije'].apply(lambda x: x.day)

df_bookings['cijena_nocenja'] = df_bookings['cijena_nocenja'].apply(lambda x: float(x.replace(',', '.')))

# this will be our variable to predict (label)
df_bookings['trajanje_dolaska'] = (df_bookings['datum_odjave'] - df_bookings['datum_dolaska']).dt.days
df_bookings['trajanje_dolaska'] = df_bookings['trajanje_dolaska'].apply(lambda x: 1 if x < 0 else x + 1)


print(df_bookings.head())



# Questions:

# 1. Why to remove 'rezervacija_id' attribute?

# 2. Check if 'datum_dolaska' is greater than 'datum_odjave'. What to do with those rows? E.g. remove them?



df_bookings.drop(["datum_dolaska", "datum_kreiranja_rezervacije", "datum_odjave", "datum_otkazivanja_rezervacije", "rezervacija_id"], axis=1, inplace=True)


print(df_bookings.head())


df_bookings = df_bookings[df_bookings["status_rezervacije"] == "Check-Out"]

df_bookings.status_rezervacije.unique()

df_bookings.drop(["status_rezervacije"], axis=1, inplace=True)
df_bookings.shape

le = LabelEncoder()
df_bookings['zemlja_gosta'] = le.fit_transform(df_bookings['zemlja_gosta'])


print(df_bookings.head())
print(df_bookings.describe())


# move label column to the last position
temp_cols = df_bookings.columns.tolist()
index = df_bookings.columns.get_loc("trajanje_dolaska")
new_cols = temp_cols[0:index] + temp_cols[index+1:] + temp_cols[index:index+1] 
df_bookings = df_bookings[new_cols]

variables_nr = len(df_bookings.columns)


print(df_bookings.head())
print(df_bookings["trajanje_dolaska"])
print(df_bookings.columns)




bookings = df_bookings.values

X = bookings[:, 0:(variables_nr - 2)]
Y = bookings[:, variables_nr - 1]

bookings.shape

# 2. Machine Learning

seed = 8

models = []
models.append(('LR', LogisticRegression()))
models.append(('RF', RandomForestRegressor()))

results = []
names = []

for name, model in models:
	kfold = model_selection.KFold(n_splits=5, shuffle=True)
	cv_results = model_selection.cross_val_score(model, X, Y, cv=kfold)
	
	print(cv_results)

	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	
	print(msg)


# boxplot algorithm comparison

fig = plt.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show()




# https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV

X_train, X_test, y_train, y_test = model_selection.train_test_split(X, Y, test_size = 0.3, random_state = seed)

rfc = RandomForestRegressor()

forest_params = [{'n_estimators': list(range(10, 50, 100)), 'max_depth': list(range(10, 50, 100)), 'max_features': list(range(3, 10))}]

clf = model_selection.GridSearchCV(rfc, forest_params, cv = 5)

clf.fit(X_train, y_train)

print("Best parameters for Random Forest:")
print(clf.best_params_)
print(clf.best_score_)



rfc = RandomForestRegressor(n_estimators=10, max_depth=10, max_features=7)

model.fit(X, Y)

importance = model.feature_importances_

for i,v in enumerate(importance):
    print('Feature: %s, Score: %.5f' % (df_bookings.columns[i],v))

plt.bar([x for x in df_bookings.columns[:13]], importance)
plt.show()



rfc = LogisticRegression()

model.fit(X, Y)

importance = model.feature_importances_
# summarize feature importance
for i,v in enumerate(importance):
    print('Feature: %s, Score: %.5f' % (df_bookings.columns[i],v))
# plot feature importance
plt.bar([x for x in df_bookings.columns[:13]], importance)
plt.show()





